<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ent="http://soapserver.com/entities"
	xmlns:requtil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.RequestUtil"
	exclude-result-prefixes="ent requtil">
	
	<xsl:output method="text"/>
	
	<xsl:template match="/">
		<xsl:variable name="countryName" select="ent:HotelsRequest/ent:Country"/>
		<xsl:variable name="cityName" select="ent:HotelsRequest/ent:City"/>
		<xsl:variable name="language" select="ent:HotelsRequest/ent:Language"/>
		<xsl:variable name="hotelName" select="ent:HotelsRequest/ent:Hotel"/>
		<xsl:variable name="category" select="ent:HotelsRequest/ent:Category"/>
		
		
		<xsl:choose>
			<xsl:when test="$countryName = 'Fake'">
				<xsl:text>Fake is not a country</xsl:text>
				<xsl:message>
					<xsl:value-of select="requtil:overrideResponse()"/>
				</xsl:message>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>select co.name as country_name, ci.name as city_name, hn.name as hotel_name, h.phone as hotel_phone, h.fax as fax, h.email as email, h.url, h.check_in, h.check_out, h.latitude, h.longitude from (countries as co join cities as ci on co.country_id=ci.country_id) join hotels as h on h.city_id=ci.city_id join hotel_name as hn on hn.hotel_id=h.id join languages as l on hn.lang_id=l.language_id</xsl:text>
				<xsl:if test="string-length($countryName) &gt; 0">
					<xsl:text> where co.name="</xsl:text>
					<xsl:value-of select="$countryName"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="string-length($cityName) &gt; 0">
					<xsl:text> and ci.name="</xsl:text>
					<xsl:value-of select="$cityName"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="string-length($language) &gt; 0">
					<xsl:text> and l.code="</xsl:text>
					<xsl:value-of select="$language"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="string-length($hotelName) &gt; 0">
					<xsl:text> and hn.name like '%</xsl:text>
					<xsl:value-of select="$hotelName"/>
					<xsl:text>%'</xsl:text>
				</xsl:if>
				<xsl:if test="string-length($category) &gt; 0">
					<xsl:text> and h.category_id="</xsl:text>
					<xsl:value-of select="$category"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>