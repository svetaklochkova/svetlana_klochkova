<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ent="http://soapserver.com/entities"
	xmlns:sutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.StringUtil"
	xmlns:queryutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.SqlQueriesUtil"
	exclude-result-prefixes="ent sutil queryutil">

	<xsl:output method="xml" />

	<xsl:template match="/">

		<ent:HotelsResponse>
			<xsl:for-each select="Result/Entry">
				<xsl:variable name="cityName" select="city_name" />
				<xsl:variable name="hotelName" select="hotel_name" />
				<xsl:variable name="phone" select="hotel_phone" />
				<xsl:variable name="fax" select="fax" />
				<xsl:variable name="email" select="email" />
				<xsl:variable name="url" select="url" />
				<xsl:variable name="checkIn" select="checkIn" />
				<xsl:variable name="checkOut" select="checkOut" />
				<xsl:variable name="latitude" select="latitude" />
				<xsl:variable name="longitude" select="longitude" />

				<ent:Country>
					<ent:Name>
						<xsl:value-of select="country_name" />
					</ent:Name>
					<ent:City>
						<xsl:value-of select="$cityName" />
					</ent:City>
					<ent:Hotel>
						<xsl:value-of select="$hotelName" />
					</ent:Hotel>
					<ent:Phone>
						<xsl:value-of select="$phone" />
					</ent:Phone>
					<ent:Fax>
						<xsl:value-of select="$fax" />
					</ent:Fax>
					<ent:Email>
						<xsl:value-of select="$email" />
					</ent:Email>
					<ent:Url>
						<xsl:value-of select="$url" />
					</ent:Url>
					<ent:CheckIn>
						<xsl:value-of select="$checkIn" />
					</ent:CheckIn>
					<ent:CheckOut>
						<xsl:value-of select="$checkOut" />
					</ent:CheckOut>
					<ent:Latitude>
						<xsl:value-of select="$latitude" />
					</ent:Latitude>
					<ent:Longitude>
						<xsl:value-of select="$longitude" />
					</ent:Longitude>
				</ent:Country>
			</xsl:for-each>
		</ent:HotelsResponse>
	</xsl:template>

</xsl:stylesheet>