CREATE DATABASE IF NOT EXISTS `service_static` DEFAULT CHARACTER SET utf8;
use `service_static`;

DROP TABLE IF EXISTS `hotel_images`;
DROP TABLE IF EXISTS `hotel_image_type`;
DROP TABLE IF EXISTS `hotel_name`;
DROP TABLE IF EXISTS `hotels`;
DROP TABLE IF EXISTS `cities`;
DROP TABLE IF EXISTS `countries`;
DROP TABLE IF EXISTS `currencies`;
DROP TABLE IF EXISTS `languages`;

create table `currencies` (
  `id` serial not null,
  `code` varchar(3) not null,
  primary key (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table `countries` (
  `country_id` serial not null,
  `name` varchar(300) not null,
  `currency_id` bigint unsigned not null,
  foreign key (`currency_id`) references currencies(`id`),
  primary key (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table `cities` (
  `city_id` serial not null,
  `country_id` bigint unsigned not null,
  `name` varchar(300) not null,
  `active` tinyint(1) not null,
  foreign key (`country_id`) references countries(`country_id`),
  primary key (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `languages` (
  `language_id` tinyint(3) NOT NULL,
  `code` varchar(2) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hotels` (
  `id` serial NOT NULL,
  `code` varchar(32) DEFAULT NULL,
  `city_id` bigint unsigned NOT NULL,
  `country_id` bigint unsigned NOT NULL,
  `category_id` enum('1','2','3','4','5') DEFAULT NULL,
  `chain_id` bigint(20) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `check_in` time DEFAULT NULL,
  `check_out` time DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `default_image` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  foreign key (`city_id`) references cities(`city_id`),  
  foreign key (`country_id`) references countries(`country_id`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hotel_name` (
  `hotel_id` bigint unsigned NOT NULL DEFAULT '0',
  `lang_id` tinyint(3) NOT NULL,
  `name` varchar(500) NOT NULL,
  foreign key (`lang_id`) references languages(`language_id`),
  foreign key (`hotel_id`) references hotels(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hotel_image_type` (
  `id` serial NOT NULL,
  `type_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hotel_images` (
  `id` serial NOT NULL,
  `hotel_id` bigint unsigned NOT NULL,
  `type_id` bigint unsigned NOT NULL,
  `url` varchar(600) DEFAULT NULL,
  `thumbnail_url` varchar(600) DEFAULT NULL,
   foreign key (`hotel_id`) references hotels(`id`),
   foreign key (`type_id`) references hotel_image_type(`id`),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


insert into `currencies`(code) values('EUR');
SET @eur = LAST_INSERT_ID();
insert into `currencies`(code) values('BYN');
SET @byn = LAST_INSERT_ID();
insert into `currencies`(code) values('pln');
SET @pln = LAST_INSERT_ID();

insert into `countries`(name, currency_id) values('Spain', @eur);
SET @country = LAST_INSERT_ID();
insert into `cities`(name, country_id, active) values('Madrid', @country, 1);
insert into `cities`(name, country_id, active) values('Barcelona', @country, 1);

insert into `countries`(name, currency_id) values('Belarus', @byn);
SET @country = LAST_INSERT_ID();
insert into `cities`(name, country_id, active) values('Minsk', @country, 1);
insert into `cities`(name, country_id, active) values('Mozyr', @country, 1);

insert into `countries`(name, currency_id) values('Poland', @pln);
SET @country = LAST_INSERT_ID();
insert into `cities`(name, country_id, active) values('Warsaw', @country, 1);
insert into `cities`(name, country_id, active) values('Krakow', @country, 1);

insert into `countries` (name, currency_id) values ('Fake', 1);


insert into languages (language_id, code, name) values (1,'ru','russian');
insert into languages (language_id, code, name) values (2,'en','english');
insert into languages (language_id, code, name) values (3,'by','belarus');


insert into hotels (id, code, city_id, country_id, category_id, chain_id, phone, fax, url, check_in, check_out, email, zip, latitude, longitude, active, default_image, created)
	values (1,'Orbita',3,2,'3',1,'+375 17 206 7781','+375 17 202 2658','www.orbita-hotel.com','14:00:00','12:00:00','reservation@orbita-hotel.com',NULL,53.908231,27.493887,'',NULL,NULL);
insert into hotels (id, code, city_id, country_id, category_id, chain_id, phone, fax, url, check_in, check_out, email, zip, latitude, longitude, active, default_image, created)
	values (2,'Marriott',3,2,'5',1,'+375 17 279 30 00','+375 17 279 31 00','www.marriott.com','14:00:00','12:00:00','reservation@hotel.com',NULL,53.908231,27.493887,'',NULL,NULL);
insert into hotels (id, code, city_id, country_id, category_id, chain_id, phone, fax, url, check_in, check_out, email, zip, latitude, longitude, active, default_image, created)
	values (3,'Tyrist',4,2,'3',1,'+375 232 580 201','+375 232 575 903','http://www.gomeltourist.com','13:00:00','13:00:00','priem@gomeltourist.com ',NULL,52.260000,30.590000,'',NULL,NULL);

insert into hotel_name (hotel_id, lang_id, name) values (1,1,'Orbita');
insert into hotel_name (hotel_id, lang_id, name) values (2,1,'Marriott');
insert into hotel_name (hotel_id, lang_id, name) values (3,1,'Tyrist Hilton');

insert into hotel_image_type (id, type_name) values (1,'hotel');
insert into hotel_image_type (id, type_name) values (2,'hostel');
insert into hotel_image_type (id, type_name) values (3,'apartment');

insert into hotel_images (id, hotel_id, type_id, url, thumbnail_url) values (1,1,1,'url1','url1');
insert into hotel_images (id, hotel_id, type_id, url, thumbnail_url) values (2,2,1,'url2','url2');
insert into hotel_images (id, hotel_id, type_id, url, thumbnail_url) values (3,3,2,'url3','url3');
insert into hotel_images (id, hotel_id, type_id, url, thumbnail_url) values (4,2,1,'url4','url4');
insert into hotel_images (id, hotel_id, type_id, url, thumbnail_url) values (5,2,1,'url5','url5');
insert into hotel_images (id, hotel_id, type_id, url, thumbnail_url) values (6,2,1,'url5','url5');