/*1 task*/
select count(*) as count 
from gpt_hotel, gpt_location, gpt_location_name 
where gpt_hotel.CITY_ID=gpt_location.ID and gpt_location.id=gpt_location_name.LOCATION_ID and gpt_location_name.name='Minsk';
/*2 task*/
select gpt_hotel_name.name as Hotel 
from gpt_hotel_name join gpt_hotel on gpt_hotel.id=gpt_hotel_name.hotel_id 
where active=b'1' and NAME like'%Hilton%';
/*3 task*/
select gpt_location.code as CITY, gpt_hotel_name.NAME, PHONE, EMAIL, gpt_hotel.LATITUDE, gpt_hotel.LONGITUDE
from ((gpt_hotel join gpt_location on gpt_location.id = gpt_hotel.city_id) join gpt_hotel_name on gpt_hotel.id=gpt_hotel_name.HOTEL_ID) join gpt_location_name on gpt_location.id=gpt_location_name.LOCATION_ID
where gpt_location_name.NAME='Minsk' and gpt_hotel.active=b'1';
/*4 task*/
select gpt_hotel_name.NAME
from (gpt_hotel join gpt_hotel_images on gpt_hotel.id=gpt_hotel_images.HOTEL_ID) join gpt_hotel_name on gpt_hotel.id=gpt_hotel_name.HOTEL_ID
group by gpt_hotel_images.HOTEL_ID
having count(gpt_hotel_images.HOTEL_ID)>3;
/*5 task*/
select concat_ws(",", gpt_hotel_name.NAME, URL, gpt_location_type.TYPE_NAME, gpt_location.CODE) as Hotel
from gpt_hotel_name, gpt_hotel, gpt_location, gpt_location_type
where gpt_hotel.id=gpt_hotel_name.HOTEL_ID and gpt_hotel.CITY_ID=gpt_location.ID and gpt_location.TYPE_ID=gpt_location_type.ID;
/*6 task*/
select max(ID) from gpt_hotel_images;
/*7 task*/
select gpt_location_name.name
from gpt_location_name, gpt_language
where gpt_location_name.LANG_ID=gpt_language.id and gpt_language.CODE='ru';