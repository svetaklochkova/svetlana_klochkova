<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:h="HouseChema"
    xmlns:i="HouseInfo" xmlns:r="RoomsChema">
    <xsl:template match="/">
        <AllRooms>
            <xsl:for-each select="//h:House">
                <xsl:variable name="address" select="concat(@City, '/', i:Address, '/')"/>
                <xsl:variable name="countRoomsInHouse" select="count(descendant::r:Room)"/>
                <xsl:for-each select="child::h:Blocks">
                    <Block><xsl:value-of select="//@number"/></Block>
                    <!--xsl:for-each select="//r:Room">
                    <Room>
                        <Address>
                            <xsl:value-of select="concat($address, '/')"/>
                        </Address>
                        <HouseRoomsCount>
                            <xsl:value-of select="$countRoomsInHouse"/>
                        </HouseRoomsCount>
                    </Room>
                </xsl:for-each-->
                </xsl:for-each>
            </xsl:for-each>
        </AllRooms>
    </xsl:template>
</xsl:stylesheet>
