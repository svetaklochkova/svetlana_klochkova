<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <Guests>
         <xsl:call-template name="string">           
             <xsl:with-param name="str" select="."/>           
         </xsl:call-template>
        </Guests>
    </xsl:template>
    
    <xsl:template name="string">          
        <xsl:param name="str"/>        
        <xsl:choose>
            <xsl:when test="substring-before($str,'#')">
                <Guest> 
                    <xsl:call-template name="splitRow">  
                        <xsl:with-param name="strRow" select="substring-before($str,'#')" />
                    </xsl:call-template>
                </Guest> 
                <xsl:call-template name="string">           
                    <xsl:with-param name="str" select="substring-after($str,'#')"/>           
                </xsl:call-template>  
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="splitRow">          
        <xsl:param name="strRow"/>
        <xsl:param name="Type" select="substring-before($strRow,'|')"/> 
        <xsl:param name="strWithoutType" select="substring-after($strRow,'|')"/>  
        <xsl:param name="age" select="substring-before($strWithoutType,'|')"/>
        <xsl:param name="strWithoutAge" select="substring-after($strWithoutType,'|')"/> 
        <xsl:param name="nationalty" select="substring-before($strWithoutAge,'|')"/> 
        <xsl:param name="strWithoutNationalty" select="substring-after($strWithoutAge,'|')"/> 
        <xsl:param name="gender" select="substring-before($strWithoutNationalty,'|')"/> 
        <xsl:param name="strWithoutGender" select="substring-after($strWithoutNationalty,'|')"/>
        <xsl:param name="name" select="substring-before($strWithoutGender,'|')"/> 
        <xsl:param name="address" select="substring-after($strWithoutGender,'|')"/> 
        <xsl:attribute name = "Age"><xsl:value-of select="$age" /></xsl:attribute>
        <xsl:attribute name = "Nationalty"><xsl:value-of select="$nationalty" /></xsl:attribute>
        <xsl:attribute name = "Gender"><xsl:value-of select="$gender" /></xsl:attribute>
        <xsl:attribute name = "Name"><xsl:value-of select="$name" /></xsl:attribute>
        <Type>
            <xsl:value-of select="$Type" />
        </Type>    
        <Profile>
            <Address>
                <xsl:value-of select="$address" /></Address>
        </Profile>
    </xsl:template>
</xsl:stylesheet>
