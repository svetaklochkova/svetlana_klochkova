<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:key name="CAPITAL_groups" match="item" use="substring(@Name, 1, 1)"/>

    <xsl:template match="list">
        <list>
            <xsl:apply-templates
                select="item[generate-id(.) = generate-id(key('CAPITAL_groups', substring(@Name, 1, 1)))]">
                <xsl:sort select="@Name"/>
            </xsl:apply-templates>
        </list>
    </xsl:template>

    <xsl:template match="item">
        <capital value="{substring(@Name,1,1)}">
            <xsl:for-each select="key('CAPITAL_groups', substring(@Name, 1, 1))">
                <xsl:variable name="a" select="@Name"/>
                <name>
                    <xsl:value-of select="$a"/>
                </name>
            </xsl:for-each>
        </capital>
    </xsl:template>

</xsl:stylesheet>
